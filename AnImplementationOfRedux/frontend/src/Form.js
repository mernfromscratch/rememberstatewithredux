import './Form.css';

const Form = ({visible, setVisible, user, setUser, sendToBack}) => {

    if (!visible) {
        return null;
    }

    const makeInputs = () => (
        <table>
            <tbody>
                <tr>
                    <td>
                        First name
                    </td>
                    <td>
                        <input type='text' name='firstName' value={user.firstName} onChange={e => setUser({...user, [e.target.name]: e.target.value})}/>
                    </td>
                </tr>
                <tr>
                    <td>
                        Last name
                    </td>
                    <td>
                        <input type='text' name='lastName' value={user.lastName} onChange={e => setUser({...user, [e.target.name]: e.target.value})}/>
                    </td>
                </tr>
                <tr>
                    <td>
                        Age
                    </td>
                    <td>
                        <input type='number' min={0} name='age' value={user.age} onChange={e => setUser({...user, [e.target.name]: e.target.value})}/>
                    </td>
                </tr>
            </tbody>
        </table>
    )

    return (
        <div className='modal'>
            <div className='modalContent'>
                <div className='modalHeader'>
                    Add a user
                </div>
                <div className='modalBody'>
                    {makeInputs()}
                </div>
                <div className='modalFooter'>
                    <button onClick={sendToBack}>
                        Submit
                    </button>
                    <button onClick={() => setVisible(false)}>
                        Cancel
                    </button>
                </div>
            </div>
        </div>
    );
};

export default Form;