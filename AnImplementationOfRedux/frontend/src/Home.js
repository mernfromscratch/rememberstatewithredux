import {useState, useEffect} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getUsersAction, postUserAction, putUserAction } from './redux/actions/userActions';

import './Home.css';
import List from './List';
import Form from './Form';

const Home = () => {

    const [modalVisible, setModalVisible] = useState(false);
    const [user, setUser] = useState({firstName: "", lastName: "", age: 0});

    const {userState} = useSelector(state => state);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getUsersAction());
    }, []);

    const openModalAndResetUser = () => {
        setModalVisible(true); 
        setUser({firstName: "", lastName: "", age: 0});
    };

    const createNewUser = () => {
        dispatch(postUserAction(user));
        setModalVisible(false);
        setUser({firstName: "", lastName: "", age: 0});
    }

    const putUser = () => {
        dispatch(putUserAction(user));
        setModalVisible(false);
        setUser({firstName: "", lastName: "", age: 0});
    }

    return (
        <div>
            <div className='center'>
                <div className='addButton' onClick={() => openModalAndResetUser()}>
                    Add
                </div>
            </div>
            <div className='showError'>
                {userState.error ? JSON.stringify(userState.error) : ""}
            </div>
            <Form visible={modalVisible} setVisible={setModalVisible} user={user} setUser={setUser} sendToBack={user._id ? putUser : createNewUser} />
            <div className={'center'}>
                <List setUser={setUser} setModalVisible={setModalVisible} />
            </div>
        </div>
    )
};

export default Home;