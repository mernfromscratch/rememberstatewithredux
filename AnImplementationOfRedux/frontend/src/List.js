import {useSelector, useDispatch} from 'react-redux';
import {deleteUserAction} from './redux/actions/userActions';
import './List.css';

const List = ({setUser, setModalVisible}) => {
    const {userState} = useSelector(state => state);
    const dispatch = useDispatch();

    const deleteUser = (_id) => {
        dispatch(deleteUserAction(_id));
    }

    const editUser = (user) => {
        setUser(user);
        setModalVisible(true);
    };

    return (
        <table className='userTable'>
            <thead>
                <tr>
                    <th>
                        First name
                    </th>
                    <th>
                        Last name
                    </th>
                    <th>
                        Age
                    </th>
                    <th>
                        Actions
                    </th>
                </tr>
            </thead>
            <tbody>
                {userState.users.map(user => (
                    <tr key={user._id}>
                        <td>
                            {user.firstName}
                        </td>
                        <td>
                            {user.lastName}
                        </td>
                        <td>
                            {user.age}
                        </td>
                        <td>
                            <button className='action' onClick={() => editUser(user)}>
                                Edit
                            </button>
                            <button className='action' onClick={() => deleteUser(user._id)}>
                                Delete
                            </button>
                        </td>
                    </tr>
                ))}
            </tbody>
        </table>
    );
};

export default List;