import {
    GET_USERS, GET_USERS_SUCCESS, GET_USERS_ERROR,
    GET_USER, GET_USER_SUCCESS, GET_USER_ERROR,
    POST_USER, POST_USER_SUCCESS, POST_USER_ERROR,
    PUT_USER, PUT_USER_SUCCESS, PUT_USER_ERROR,
    DELETE_USER, DELETE_USER_SUCCESS, DELETE_USER_ERROR
} from '../actions/userActionTypes';

import {sortTwoUsers} from '../../helperFunctions';

const initialState = {
    users: [],
    loading: true,
    updating: false,
    error: null
}


export default function userReducer(state = initialState, action) {
    switch (action.type) {
        case GET_USERS:
            return { ...state, loading: true };
        case GET_USERS_SUCCESS:
            return { ...state, users: action.payload.sort(sortTwoUsers), loading: false};
        case GET_USERS_ERROR:
            return { ...state, loading: false, error: action.payload }

        case GET_USER:
            return {...state, loading: true};
        case GET_USER_SUCCESS:
            let newUsersGet = state.users.slice(0);
            newUsersGet.push(action.payload);
            return {...state, users: newUsersGet.sort(sortTwoUsers), loading: false};
        case GET_USER_ERROR:
            return {...state, loading: false, error: action.payload};

        case POST_USER:
            return { ...state, updating: true};
        case POST_USER_SUCCESS:
            let newUsersPost = state.users.slice(0);
            newUsersPost.push(action.payload);
            return { ...state, users: newUsersPost.sort(sortTwoUsers), updating: false};
        case POST_USER_ERROR:
            return { ...state, updating: false, error: action.payload};

        case PUT_USER:
            return { ...state, updating: true};
        case PUT_USER_SUCCESS:
            let newUsersPut = state.users.slice(0).filter(user => !(user._id === action.payload._id));
            newUsersPut.push(action.payload);
            return { ...state, users: newUsersPut.sort(sortTwoUsers), updating: false};
        case PUT_USER_ERROR:
            return { ...state, updating: false, error: action.payload};

        case DELETE_USER:
            return { ...state, updating: true};
        case DELETE_USER_SUCCESS:
            let newUsersDelete = state.users.slice(0).filter(user => !(user._id === action.payload._id))
            return { ...state, users: newUsersDelete.sort(sortTwoUsers), updating: false};
        case DELETE_USER_ERROR:
            return { ...state, updating: false, error: action.payload};
    
        default:
            return state;
    }
}