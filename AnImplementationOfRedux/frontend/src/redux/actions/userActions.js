import {
    GET_USERS, GET_USERS_SUCCESS, GET_USERS_ERROR,
    GET_USER, GET_USER_SUCCESS, GET_USER_ERROR,
    POST_USER, POST_USER_SUCCESS, POST_USER_ERROR,
    PUT_USER, PUT_USER_SUCCESS, PUT_USER_ERROR,
    DELETE_USER, DELETE_USER_SUCCESS, DELETE_USER_ERROR
} from './userActionTypes';

import {
    userGetAll,
    userGetOne,
    userPostOne,
    userPutOne,
    userDeleteOne
} from '../api/userApiCalls';

export const getUsersAction = () => async dispatch => {
    dispatch({ type: GET_USERS });
    userGetAll()
        .then((users) => dispatch({
            type: GET_USERS_SUCCESS,
            payload: users.data
        }))
        .catch((err) => dispatch({
            type: GET_USERS_ERROR,
            payload: err
        }))
};

export const getUserAction = (_id) => async dispatch => {
    dispatch({ type: GET_USER });
    userGetOne(_id)
        .then(user => dispatch({
            type: GET_USER_SUCCESS,
            payload: user.data
        }))
        .catch((err) => dispatch({
            type: GET_USER_ERROR,
            payload: err
        }))
};

export const postUserAction = (user) => async dispatch => {
    dispatch({ type: POST_USER });
    userPostOne(user)
        .then(newUser => dispatch({
            type: POST_USER_SUCCESS,
            payload: newUser.data
        }))
        .catch((err) => dispatch({
            type: POST_USER_ERROR,
            payload: err
        }))
};

export const putUserAction = (user) => async dispatch => {
    dispatch({ type: PUT_USER });
    userPutOne(user)
        .then(changedUser => dispatch({
            type: PUT_USER_SUCCESS,
            payload: changedUser.data
        }))
        .catch(err => dispatch({
            type: PUT_USER_ERROR,
            payload: err
        }))
};

export const deleteUserAction = (_id) => async dispatch => {
    dispatch({ type: DELETE_USER });
    userDeleteOne(_id)
        .then(user => dispatch({
            type: DELETE_USER_SUCCESS,
            payload: user.data
        }))
        .catch((err) => dispatch({
            type: DELETE_USER_ERROR,
            payload: err
        }))
};
