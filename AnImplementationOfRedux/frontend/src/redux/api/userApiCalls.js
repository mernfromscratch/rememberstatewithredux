import axios from 'axios';

export const userGetAll = async () => {
    try {
        const users = await axios.get(process.env.REACT_APP_BACKENDURL + "/users/all");
        return users;
    } catch (error) {
        return error;
    }
};

export const userGetOne = async (_id) => {
    try {
        const user = await axios.get(process.env.REACT_APP_BACKENDURL + "user/" + _id);
        return user;
    } catch (error) {
        return error;
    }
}

export const userPostOne = async (user) => {
    try {
        const userPost = await axios.post(`${process.env.REACT_APP_BACKENDURL}user/`, user);
        return userPost;
    } catch (error) {
        return error;
    }
}

export const userPutOne = async (user) => {
    try {
        const userPut = await axios.put(`${process.env.REACT_APP_BACKENDURL}user/${user._id}`, user);
        return userPut;
    } catch (error) {
        return error;
    }
}

export const userDeleteOne = async (_id) => {
    try {
        const userDel = await axios.delete(process.env.REACT_APP_BACKENDURL + "user/" + _id);
        return userDel;
    } catch (error) {
        return error;
    }
}