export const sortTwoUsers = (user1, user2) => {
    if (user1.lastName < user2.lastName) {return -1}
    else if (user1.lastName > user2.lastName) {return 1};
    return 0;
}