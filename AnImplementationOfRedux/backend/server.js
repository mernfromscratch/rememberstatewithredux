const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');


let app = express();


// MongoDB
const mongoUrl = "mongodb+srv://totesfortutorials:totesfortutorials@cluster0.whqxa.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
mongoose.connect(mongoUrl, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
})
.then(() => {
    console.log('DB CONNECTED');
})
.catch(e => console.log(`DB CONNECTION ERROR: ${e}`));


// Middlewares and routes
app.use(cors());
app.use(express.json({limit: '20mb'}));
app.use('/api', require('./routes/users'));


const port = process.env.PORT || 8000;

app.listen(port, () => console.log(`Listening intensely on port ${port}`));