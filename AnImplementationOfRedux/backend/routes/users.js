const express = require('express');
const router = express.Router();

const {
    getAll,
    getOne,
    createOne,
    updateOne,
    deleteOne
} = require("../controllers/users");

router.get('/users/all', getAll);
router.get('/user/:_id', getOne);
router.put('/user/:_id', updateOne);
router.post('/user/', createOne);
router.delete('/user/:_id', deleteOne);

module.exports = router;