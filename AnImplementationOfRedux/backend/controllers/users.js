const User = require("../models/User");

exports.getAll = async (req, res) => {
    const users = await User.find({}).exec();
    res.json(users);
};

exports.getOne = async (req, res) => {
    try {
        const user = await User.findById(req.params._id).exec();
        res.json(user);
    } catch (error) {
        res.status(500).send(error.message);
    }
};

exports.updateOne = async (req, res) => {
    try {
        const user = await User.findByIdAndUpdate(req.params._id, req.body, {new: true}).exec();
        res.json(user);
    } catch (error) {
        res.status(500).send(error.message);
    }
};

exports.createOne = async (req, res) => {
    try {
        const user = await new User(req.body).save();
        res.json(user);
    } catch (error) {
        res.status(500).send(error.message);
    }
};

exports.deleteOne = async (req, res) => {
    try {
        const user = await User.findByIdAndDelete(req.params._id).exec();
        res.json(user);
    } catch (error) {
        res.status(500).send(error.message);
    }
};